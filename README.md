## Thesis

This LaTeX project is the source used for generating the PDF for my thesis.

Project Structure:
* chapters/
> contains LaTeX code for each chapter.
* code/
> contains the pseudo-code displayed in the PDF.
* images/
> contains the images used in the PDF.
* results/
> contains the raw data generated during the experimentation and used for generating the images.


### Spell-checking

In order to spell check this latex project you use the following command:
```shell script
make spell
```
If a word should be part of the dictionary of allowed words you just need to include it in
`dictionary.txt` .

### PDF building

This project uses the following XeTeX version:
```shell script
yagdo@thunderbolt:~/PycharmProjects/thesis$ xelatex --version
XeTeX 3.14159265-2.6-0.99998 (TeX Live 2017/Debian)
```
In order to generate the PDF file from the source you need to run the following command, which will execute the 
latex compilation.
```shell script
make build
```

After generating the PDF you can run a `make clean` command to clean the repository from all the aux files generated.

