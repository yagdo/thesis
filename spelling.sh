#!/usr/bin/env bash

excluded=$(cat dictionary.txt | tr '\n' '|');

#hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/intro.tex" | grep -Ev "$excluded";
#hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/theory.tex" | grep -Ev "$excluded";
#hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/papers.tex" | grep -Ev "$excluded";
#hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/algorithms.tex" | grep -Ev "$excluded";
#hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/experiment_part_1.tex" | grep -Ev "$excluded";
#hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/experiment_part_2.tex" | grep -Ev "$excluded";
#hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/experiment_part_3.tex" | grep -Ev "$excluded";
#hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/experiment_part_4.tex" | grep -Ev "$excluded";
#hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/experiment_part_5.tex" | grep -Ev "$excluded";
#hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/future.tex" | grep -Ev "$excluded";
hunspell -d el_GR,en_GB,en_US -l -t -i utf-8 "chapters/appendix.tex" | grep -Ev "$excluded";
echo ">>Spelling is completed";