.PHONY: clean clean-test clean-pyc clean-build docs help
.DEFAULT_GOAL := help

define BROWSER_PYSCRIPT
import os, webbrowser, sys

try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

# If the first argument is "run"...
ifeq (spell,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

BROWSER := python -c "$$BROWSER_PYSCRIPT"

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-aux ## cleans up

clean-aux: ## remove aux logs etc
	find . -name '*.aux' -exec rm -f {} +
	find . -name 'thesis.bbl' -exec rm -f {} +
	find . -name 'thesis.blg' -exec rm -f {} +
	find . -name 'thesis.log' -exec rm -f {} +
	find . -name 'thesis.out' -exec rm -f {} +
	find . -name 'thesis.idx' -exec rm -f {} +
	find . -name 'thesis.ilg' -exec rm -f {} +
	find . -name 'thesis.ind' -exec rm -f {} +
	find . -name 'thesis.toc' -exec rm -f {} +
	find . -name 'thesis.synctex.gz' -exec rm -f {} +
	rm -rf out/

build:  ## Builds the pdf
	-xelatex -file-line-error -interaction=nonstopmode -synctex=1 thesis.tex
	-bibtex thesis
	-xelatex -file-line-error -interaction=nonstopmode -synctex=1 thesis.tex
	-xelatex -file-line-error -interaction=nonstopmode -synctex=1 thesis.tex


test-build:  ## Builds the pdf
	-xelatex -file-line-error -interaction=nonstopmode -synctex=1 thesis.tex


spell:  ## Spell checks the latex sources
	bash ./spelling.sh

