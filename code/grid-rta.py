def grid_rta(W, GridIndex, q, k):
    # list containing the weights the rtopk returns
    r_top_k_result = list()
    # initial threshold is infinite since its not set
    threshold = infinite
    for w in W:
        item_score = w @ q
        # similarly to rta we calculate the top_k query only if
        # the score of q doesn't cross the threshold
        if item_score < threshold:
            buffer = list()
            remaining_k = k
            # we try to get the most influential data objects by
            # selecting the grids closer to the weight.
            for grid in GridIndex:
                # here we build up the buffer. By building the
                # buffer using grid by grid we avoid making
                # calculations on tuples that are not
                # influential enough
                buffer += Top_K(w, grid, q, k)
                remaining_k = remaining_k - len(buffer)
                if remaining_k <= 0:
                    # if there are enough tuple to invalidate
                    # the weight there is no need to keep
                    # iterating the GridIndex
                    break

            # if the number of tuples is less than k
            # we can add the weight at the result of the query
            if len(buffer) < k:
                r_top_k_result.append(w)

        # we get the next weight and we calculate the threshold
        # based on the worst score inside the buffer
        next_weight = get_next_weight()
        if next_weight and len(buffer) == k:
            threshold = max(buffer @ next_weight)

    return r_top_k_result