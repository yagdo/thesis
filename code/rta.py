def rta(W, S, q, k):
    # list containing the weights the rtopk returns
    r_top_k_result = list()
    # initial threshold is infinite since its not set
    threshold = infinite
    for w in W:
        # we calculate the top_k query only if
        # the score of q doesn't cross the threshold
        q_score = q @ w
        if q_score < threshold:
            buffer = Top_K(w, S, q, k)
            # if the number of tuples is less than k
            # we can add the weight at the result of the query
            if len(buffer) < k:
                r_top_k_result.append(w)

        # we get the next weight and we calculate the threshold
        # based on the worst score inside the buffer
        next_weight = get_next_weight()
        if next_weight and len(buffer) == k:
            threshold = max(buffer @ next_weight)

    return r_top_k_result