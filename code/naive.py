def naive(W, S, q, k):
    # list containing the weights the rtopk returns
    r_top_k_result = list()
    for w in W:
        # find up to k tuples from the data-set with better score
        # for a certain weight
        top_k_points = Top_K(w, S, q, k)
        # if the number of tuples found is less than K
        # then the weight belongs to the reverse top-k result
        if len(top_k_points) < k:
            r_top_k_result.append(w)

    return r_top_k_result