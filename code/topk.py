def Top_K(w, S, q, k):
    q_score = q @ w
    buffer = list()
    for p in S:
        point_score = p @ w
        # if a tuple from the data-set has a better score
        # we add it to the buffer of tuples with better
        # score than q
        if point_score < q_score:
            buffer.append(p)
        # if we have k tuples with better score there is no need
        # for us to keep processing the data-set
        if len(buffer) >= k:
            break

    return buffer