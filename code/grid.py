def layered(W, GridIndex, q, k):
    weight_matrix = list()
    for w in W:
        weight_matrix.append([w, k])

    # The matrix should look like this
    # weight_matrix = [
    #     [w_1, k],
    #     [w_2, k],
    #     [w_3, k],
    #     ...,
    #     [w_4, k],
    # ]


    for grid in GridIndex:
        # this is a flag for checking if we have to
        # prune the matrix
        should_delete = False

        for row in weight_matrix:
            w = weight_matrix[row][0]
            k_remaining = weight_matrix[row][1]
            # we maintain the number of needed tuples in order
            # to invalidate the weight from the result
            top_k_points = Top_K(w, grid, q, k_remaining)
            # updates the number of needed tuples to
            # invalidate the weight
            weight_matrix[row][1] = weight_matrix[row][1] - size(top_k_points)
            # if one of the weights need to be removed
            # then we need to prune the matrix
            if weight_matrix[row][1] <= 0:
                should_delete = True

        # we need to delete the rows where the second column
        # is less or equal to zero since we found enough tuples
        # in the data-set to invalidate the weight from being in the result.
        if should_delete:
            remove rows in weight_matrix where rows[1] <= 0

        # if the matrix is empty there are no more weights
        # to examine
        if len(weight_matrix) == 0:
            break
    # we return the weights in the weight matrix
    return [row[0] for row in weight_matrix]