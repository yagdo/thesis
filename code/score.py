def score(w, p, D):
    score_value = 0
    for d in D:
        score_value += p[d] * w[d]
    return score_value
